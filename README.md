# Genesis Online Wrapper
Node.js wrapper for the germany genesis online databases (Version 4.1).

The wrapper is currently still in an alpha version.

This is not an official adapter of the Statistisches Bundesamt.

The wrapper is structured like the original interface. Therefore parameters and
methods have not been translated into English. During the development it was terrible (e.g. addStrukturInformationen),
but the official [Manuel](https://www-genesis.destatis.de:443/genesis/misc/GENESIS-Webservices_Einfuehrung.pdf) can be used for the wrapper as well.

The Genesis Online Databases, are maintained by the Statistical
Offices of the Federation and the Länder of Germany.
The aim is to establish a common data warehouse for statistical information.

However, the http interface of the Genesis online databases is very complicated and has its peculiarities. For example, all parameters must be specified in the exact order given. To simplify this, this wrapper is developed. For the R Community there is the R Package [Wiesbaden](https://github.com/sumtxt/wiesbaden).  

Thus, various statistical offices of the Federation and the Länder use the "Genesis Online" system to make their data available:

Bund: https://www.destatis.de/DE/Startseite.html

Regionalstatistik: https://www.regionalstatistik.de/genesis/online/

Landesdatenbank NRW: https://www.landesdatenbank.nrw.de

Bildungsmonitoring: https://www.bildungsmonitoring.de/

## Installation
Npm install command:

```
$ npm install genesis-online-wrapper
```

## Initialisation
```js
const genesis = require('genesis-online-wrapper');

genesis.initWebservice({kennung: 'KENNUNG', passwort: 'PASSWORT'});
```

## Example
Get all Bundesländer with code (Amtlicher Gemeindeschlüssel).

```js
const genesis = require("genesis-online-wrapper");

genesis.initWebservice({kennung: 'KENNUNG', passwort: 'PASSWORT'});

var getBundeslaender = function () {
    var bundeslaender = [];
    genisis.rechercheService.MerkmalAuspraegungenKatalog(
        {
            name:"DLAND",
        }
    ).then(result => {
        for(var i = 0; i < result['merkmalAuspraegungenKatalogEintraege']['merkmalAuspraegungenKatalogEintraege'].length; i++){

            bundeslaender.push({
                BundeslandId: result['merkmalAuspraegungenKatalogEintraege']['merkmalAuspraegungenKatalogEintraege'][i]['code']['_text'],
                Name: result['merkmalAuspraegungenKatalogEintraege']['merkmalAuspraegungenKatalogEintraege'][i]['inhalt']['_text']
            });
        }
        console.log(bundeslaender);
    });
}
```

## Implemented Methods

The manual [Manuel](https://www-genesis.destatis.de:443/genesis/misc/GENESIS-Webservices_Einfuehrung.pdf)
for the original interface.
#### InitWebservice

| Parameter | Constraint    | Default                                       | Range |
| --- | --- |--- |--- |
| passwort  | required      | -                                             | - |
| kennung   | required      | -                                             | - |
| baseUrl   | optional      | https://www-genesis.destatis.de/genesisWS/web | *     |
| sprace    | optional      | de                                            | de, en |
| timeout   | optional      | 10000                                         | - |

```js
initWebservice(
  {
   kennung: 'kennung',
   passwort: 'passwort',
   baseUrl: 'https://www-genesis.destatis.de/genesisWS/web',
   sprace: 'de',
   timeout: '10000'
  })

```

\* Available databases (baseUrl):

* https://www.landesdatenbank.nrw.de/ldbnrwws/services
* https://www.regionalstatistik.de/genesisws/services
* https://www-genesis.destatis.de/genesisWS/web
* https://www.bildungsmonitoring.de/bildungws/services


#### ExportService
- exportService.TabellenExport(parameter)
- exportService.MerkmalInformation(parameter)
- exportService.AuspraegungInformation(parameter)

#### RechercheService
- rechercheService.MerkmalsKatalog(parameter)
- rechercheService.MerkmalAuspraegungenKatalog(parameter)
- rechercheService.AuftraegeKatalog(parameter)

#### testService
- exportService.whoami(parameter)
- exportService.MerkmalAuspraegungenKatalog(parameter)



