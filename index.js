const axios = require('axios');
var convert = require('xml-js');
const CircularJSON = require('circular-json');

var constParams = {};
var baseService;

var testService = {};
var rechercheService = {};
var exportService = {};
var wrapperService = {};

wrapperService.initWebservice = function(params){

    // force params to be an obj
    params = params || {};
    constParams.passwort = addDefault(params.passwort);
    constParams.kennung = addDefault(params.kennung);
    constParams.baseUrl = params.baseUrl || "https://www-genesis.destatis.de/genesisWS/web";
    constParams.sprache = params.sprache || "de";
    constParams.timeout = params.timeout || "10000";

    baseService = axios.create({
        baseURL: constParams.baseUrl,
        timeout: constParams.timeout
    });
};


/**
 * Converts a CSV to JSON
 * @param params {header: [], headerLine}
 * @returns {Promise<any>}
 */
wrapperService.parseCSV = function(params, data){

    return new Promise(
        function (resolve, reject) {
                let string = data.split("\n");
                let result = [];
                let exit = true;
                for (let line = 0; line < string.length; line++) {

                    var header = params.header;
                    if (!exit || string[line] == "__________") {
                        exit = false;
                        continue;
                    }
                    if (line > headerLine) {
                        var temp = string[line].split(";");
                        var obj = {};
                        for (let prop = 0; prop < temp.length; prop++) {
                            obj[header[prop]] = temp[prop];
                        }
                        result.push(obj);
                    }
                }
                    resolve(result);
            })
}




exportService.TabellenExport = function(requestParameter){

    console.log("TabellenExport", "Starting");
    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "TabellenExport";
    params = addCendentials(params);
    params.namen = addDefault(requestParameter.namen);
    params.bereich = addBereich(requestParameter.bereich);
    params.format = addDatenFormat(requestParameter.format);
    params.strukturinformation=addStrukturInformationen(requestParameter.strukturInformationen);
    params.komprimieren= addKomprimieren(requestParameter.komprimieren);
    params.transponieren=addTransponieren(requestParameter.transponieren);
    params.startjahr= addDefault(requestParameter.startJahr);
    params.endjahr= addDefault(requestParameter.endJahr);
    params.zeitscheiben=addDefault(requestParameter.zeitscheiben);
    params.regionalmerkmal=addDefault(requestParameter.regionalMerkmal);
    params.regionalschluessel=addDefault(requestParameter.regionalSchluessel);
    params.sachmerkmal=addDefault(requestParameter.sachMerkmal);
    params.sachschluessel=addDefault(requestParameter.sachSchluessel);
    params.sachmerkmal2=addDefault(requestParameter.sachMerkmal2);
    params.sachschluessel2=addDefault(requestParameter.sachSchluessel2);
    params.sachmerkmal3=addDefault(requestParameter.sachMerkmal3);
    params.sachschluessel3=addDefault(requestParameter.sachSchluessel3);
    params.stand= addStand(requestParameter.stand);
    params.auftrag = addAuftrag(requestParameter.auftrag);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('ExportService_2010', params).then(result => {
            //console.log("Result:", result);
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']['tabellen']['tabellen']['tabellenDaten']['_text']);
        });
    })
}
exportService.AuspraegungInformation = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "AuspraegungInformation";
    params = addCendentials(params);
    params.name = addDefault(requestParameter.name);
    params.bereich = addBereich(requestParameter.bereich);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('ExportService_2010', params).then(result => {
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })
};
exportService.MerkmalInformation = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "MerkmalInformation";
    params = addCendentials(params);
    params.name = addDefault(requestParameter.name);
    params.bereich = addBereich(requestParameter.bereich);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('ExportService_2010', params).then(result => {
            console.log("Raw", result);
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })
};
exportService.ErgebnisExport = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "ErgebnisExport";
    params = addCendentials(params);
    params.name = addDefault(requestParameter.name);
    params.bereich = addBereich(requestParameter.bereich);
    params.format = addDatenFormat(params);
    params.komprimieren = addKomprimieren(params);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('ExportService_2010', params).then(result => {
            console.log("Raw", result);
            resolve(result);
            //resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })
};




testService.whoami = function() {
    return new Promise(function (resolve, reject) {
        baseService.get('genesisWS/web/TestService', {
            params: {
                method: 'whoami'
            }
        }).then(function (response) {
            var result1 = convert.xml2json(response.data, {compact: true, spaces: 4});
            result1 = JSON.parse(result1);
            resolve(result1['soapenv:Envelope']['soapenv:Body']['whoamiResponse']['whoamiReturn']['_text']);
        })
            .catch(function (error) {
                console.log(error);
                reject(error);
            })
            .then(function () {
                // always executed
            });
    })
}
rechercheService.MerkmalsKatalog = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "MerkmalsKatalog";
    params = addCendentials(params);
    params.filter = addFilter(requestParameter.filter);
    params.kriterium = addKriterium(requestParameter.kriterium);
    params.typ = addTyp(requestParameter.typ);
    params.bereich = addBereich(requestParameter.bereich);
    params.listenLaenge = addListenLaenge(requestParameter.listenLaenge);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('RechercheService_2010', params).then(result => {
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })

};
rechercheService.MerkmalAuspraegungenKatalog = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "MerkmalAuspraegungenKatalog";
    params = addCendentials(params);
    params.name = addDefault(requestParameter.name);
    params.auswahl = addAuswahl(requestParameter.auswahl);

    params.kriterium = addKriterium(requestParameter.kriterium);
    params.bereich = addBereich(requestParameter.bereich);
    params.listenLaenge = addListenLaenge(requestParameter.listenLaenge);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('RechercheService_2010', params).then(result => {
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })

};
rechercheService.AuftraegeKatalog = function(requestParameter){

    // force params to be an obj
    requestParameter = requestParameter || {};
    var params = {};

    params.method = "MerkmalAuspraegungenKatalog";
    params = addCendentials(params);
    params.name = addDefault(requestParameter.name);
    params.auswahl = addAuswahl(requestParameter.auswahl);

    params.kriterium = addKriterium(requestParameter.kriterium);
    params.bereich = addBereich(requestParameter.bereich);
    params.listenLaenge = addListenLaenge(requestParameter.listenLaenge);
    params = addLanguage(params);

    return new Promise(function (resolve, reject) {
        sendRequest('RechercheService_2010', params).then(result => {
            console.log(result);
            resolve(result['soapenv:Envelope']['soapenv:Body'][params.method + 'Response'][params.method + 'Return']);
        });
    })

};









var sendRequest  = function(service,requestParams) {

    return new Promise(function (resolve, reject) {
        baseService.get(service, {params: requestParams}).then(response => {
            console.log("response");

            var parsed = responseParser(response, requestParams.method);
            resolve(parsed);
        })
            .catch(function (error) {

                console.log("------------------ ERROR ------------------");
                console.log(error);
                reject(error);
            })
            .then(function () {
            });
    })
};

var responseParser = function(response, method) {
    var response1 = convert.xml2json(response.data, {compact: true, spaces: 4});
    response1 = JSON.parse(response1);
    return response1;
};

var addCendentials = function (params) {
    params.kennung = constParams.kennung;
    params.passwort = constParams.passwort;
    return params;
}
var addLanguage = function (params) {
    params.sprache = constParams.sprache;
    return params;
}
var addDefault = function (param) {
    return param || "";
}
var addKomprimieren = function (param) {
    return param || "false";
}
var addTransponieren = function (param) {
    return param || "false";
}
var addStrukturInformationen = function (param) {
    return param || "false";
}
var addDatenFormat = function (param) {
    return param || "csv";
}
var addGrafikFormat = function (param) {
    return param || "png";
}
var addBereich = function (param) {
    return param || "Alle";
}
var addAuftrag = function (param) {
    return param || "false";
}
var addStand = function (param) {
    return param || "";
}
var addListenLaenge = function (param) {
    return param || "500";
}
var addFilter = function (param) {
    return param || "";
}
var addKriterium = function (param) {
    return param || "Code";
}
var addTyp = function (param) {
    return param || "Alle";
}
var addAuswahl = function (param) {
    return param || "";
}


module.exports.exportService = exportService;
module.exports.testService = testService;
module.exports.rechercheService = rechercheService;
module.exports.wrapperService = wrapperService;